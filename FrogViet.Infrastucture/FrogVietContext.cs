﻿using System;
using System.Collections.Generic;
using FrogViet.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FrogViet.Infrastucture

{
    public partial class FrogVietContext : DbContext
    {
        public FrogVietContext()
        {
        }

        public FrogVietContext(DbContextOptions<FrogVietContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Company> Companies { get; set; } = null!;
        public virtual DbSet<Cv> Cvs { get; set; } = null!;
        public virtual DbSet<Event> Events { get; set; } = null!;
        public virtual DbSet<Interview> Interviews { get; set; } = null!;
        public virtual DbSet<InterviewRecord> InterviewRecords { get; set; } = null!;
        public virtual DbSet<InterviewResult> InterviewResults { get; set; } = null!;
        public virtual DbSet<Job> Jobs { get; set; } = null!;
        public virtual DbSet<Plan> Plans { get; set; } = null!;
        public virtual DbSet<Post> Posts { get; set; } = null!;
        public virtual DbSet<UserAccount> UserAccounts { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=LAPTOP-QEF3DP6J;Initial Catalog=FrogViet;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>(entity =>
            {
                entity.ToTable("Company");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CodeLicense)
                    .HasColumnType("numeric(10, 0)")
                    .HasColumnName("Code_License");

                entity.Property(e => e.CompanyAddress).HasColumnName("Company_Address");

                entity.Property(e => e.CompanyDecription).HasColumnName("Company_Decription");

                entity.Property(e => e.CompanyEmail).HasColumnName("Company_Email");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(50)
                    .HasColumnName("Company_Name");

                entity.Property(e => e.CompanyPhone).HasColumnName("Company_Phone");

                entity.Property(e => e.CompanyWeb).HasColumnName("Company_Web");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Companies)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Company_UserAccounts");
            });

            modelBuilder.Entity<Cv>(entity =>
            {
                entity.ToTable("CV");

                entity.Property(e => e.Cvid).HasColumnName("CVID");

                entity.Property(e => e.Certification).HasMaxLength(50);

                entity.Property(e => e.ContactInformation)
                    .HasMaxLength(50)
                    .HasColumnName("Contact_information");

                entity.Property(e => e.CvImage).HasColumnName("CV_Image");

                entity.Property(e => e.CvName)
                    .HasMaxLength(50)
                    .HasColumnName("CV_Name");

                entity.Property(e => e.DateCreate)
                    .HasColumnType("date")
                    .HasColumnName("Date_Create");

                entity.Property(e => e.DateUpdate)
                    .HasColumnType("date")
                    .HasColumnName("Date_Update");

                entity.Property(e => e.Education).HasMaxLength(50);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Interests).HasMaxLength(50);

                entity.Property(e => e.JobTitle)
                    .HasMaxLength(50)
                    .HasColumnName("Job_Title");

                entity.Property(e => e.Language).HasMaxLength(50);

                entity.Property(e => e.Skills).HasMaxLength(50);

                entity.Property(e => e.Summary).HasMaxLength(50);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.WorkExperience)
                    .HasMaxLength(50)
                    .HasColumnName("Work_Experience");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Cvs)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CV_UserAccounts");
            });

            modelBuilder.Entity<Event>(entity =>
            {
                entity.ToTable("Event");

                entity.Property(e => e.EventId).HasColumnName("EventID");

                entity.Property(e => e.EventDate)
                    .HasColumnType("date")
                    .HasColumnName("Event_Date");

                entity.Property(e => e.EventDescription).HasColumnName("Event_Description");

                entity.Property(e => e.EventLocation).HasColumnName("Event_Location");

                entity.Property(e => e.EventTime)
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("Event_Time");

                entity.Property(e => e.EventType)
                    .HasMaxLength(50)
                    .HasColumnName("Event_Type");

                entity.Property(e => e.PlanId).HasColumnName("PlanID");

                entity.HasOne(d => d.Plan)
                    .WithMany(p => p.Events)
                    .HasForeignKey(d => d.PlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Event_Plan");
            });

            modelBuilder.Entity<Interview>(entity =>
            {
                entity.ToTable("Interview");

                entity.Property(e => e.InterviewId).HasColumnName("InterviewID");

                entity.Property(e => e.Cvid).HasColumnName("CVID");

                entity.Property(e => e.InterviewDate)
                    .HasColumnType("date")
                    .HasColumnName("Interview_Date");

                entity.Property(e => e.InterviewDecription)
                    .HasMaxLength(50)
                    .HasColumnName("Interview_Decription");

                entity.Property(e => e.InterviewLocation)
                    .HasMaxLength(50)
                    .HasColumnName("Interview_Location");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Cv)
                    .WithMany(p => p.Interviews)
                    .HasForeignKey(d => d.Cvid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Interview_CV");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Interviews)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Interview_UserAccounts");
            });

            modelBuilder.Entity<InterviewRecord>(entity =>
            {
                entity.ToTable("InterviewRecord");

                entity.Property(e => e.InterviewRecordId).HasColumnName("Interview_Record_ID");

                entity.Property(e => e.InterviewId).HasColumnName("InterviewID");

                entity.Property(e => e.QuestionAnswer).HasColumnName("Question_Answer");

                entity.HasOne(d => d.Interview)
                    .WithMany(p => p.InterviewRecords)
                    .HasForeignKey(d => d.InterviewId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InterviewRecord_Interview");
            });

            modelBuilder.Entity<InterviewResult>(entity =>
            {
                entity.ToTable("InterviewResult");

                entity.Property(e => e.InterviewResultId).HasColumnName("Interview_Result_ID");

                entity.Property(e => e.DateResult)
                    .HasColumnType("date")
                    .HasColumnName("Date_Result");

                entity.Property(e => e.InterviewId).HasColumnName("InterviewID");

                entity.Property(e => e.Result).HasMaxLength(50);

                entity.HasOne(d => d.Interview)
                    .WithMany(p => p.InterviewResults)
                    .HasForeignKey(d => d.InterviewId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InterviewResult_Interview");
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.ToTable("Job");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.JobDecription).HasColumnName("Job_Decription");

                entity.Property(e => e.JobName)
                    .HasMaxLength(50)
                    .HasColumnName("Job_Name");

                entity.Property(e => e.JobRequirement)
                    .HasMaxLength(50)
                    .HasColumnName("Job_Requirement");

                entity.Property(e => e.RecruitmentNumber).HasColumnName("Recruitment_Number");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Job_Company");
            });

            modelBuilder.Entity<Plan>(entity =>
            {
                entity.ToTable("Plan");

                entity.Property(e => e.PlanId).HasColumnName("PlanID");

                entity.Property(e => e.DateCreate)
                    .HasColumnType("date")
                    .HasColumnName("Date_Create");

                entity.Property(e => e.DateEnd)
                    .HasColumnType("date")
                    .HasColumnName("Date_End");

                entity.Property(e => e.DateStart)
                    .HasColumnType("date")
                    .HasColumnName("Date_Start");

                entity.Property(e => e.PlanName)
                    .HasMaxLength(50)
                    .HasColumnName("Plan_Name");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Plans)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Plan_UserAccounts");
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.HasKey(e => e.PosterId);

                entity.ToTable("Post");

                entity.Property(e => e.PosterId).HasColumnName("PosterID");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.PlanId).HasColumnName("PlanID");

                entity.Property(e => e.PosterDateCreate)
                    .HasColumnType("date")
                    .HasColumnName("Poster_Date_Create");

                entity.Property(e => e.PosterImage).HasColumnName("Poster_Image");

                entity.Property(e => e.PosterName)
                    .HasMaxLength(50)
                    .HasColumnName("Poster_Name");

                entity.Property(e => e.RangeOfSalary)
                    .HasMaxLength(50)
                    .HasColumnName("Range_Of_Salary");

                entity.Property(e => e.Skills).HasMaxLength(50);

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.JobId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Post_Job");

                entity.HasOne(d => d.Plan)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.PlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Post_Plan");
            });

            modelBuilder.Entity<UserAccount>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.DateCreate)
                    .HasColumnType("date")
                    .HasColumnName("Date_Create");

                entity.Property(e => e.DateUpdate)
                    .HasColumnType("date")
                    .HasColumnName("Date_Update");

                entity.Property(e => e.IdcardNumber).HasColumnName("IDCard_Number");

                entity.Property(e => e.UserBirth)
                    .HasColumnType("date")
                    .HasColumnName("User_Birth");

                entity.Property(e => e.UserEmail)
                    .HasMaxLength(50)
                    .HasColumnName("User_Email");

                entity.Property(e => e.UserImage).HasColumnName("User_Image");

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .HasColumnName("User_Name");

                entity.Property(e => e.UserPassword)
                    .HasMaxLength(50)
                    .HasColumnName("User_Password");

                entity.Property(e => e.UserPhone).HasColumnName("User_Phone");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
