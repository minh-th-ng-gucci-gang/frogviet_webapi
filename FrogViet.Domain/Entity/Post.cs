﻿using System;
using System.Collections.Generic;

namespace FrogViet.Domain.Entity
{
    public partial class Post
    {
        public int PosterId { get; set; }
        public string PosterImage { get; set; } = null!;
        public string PosterName { get; set; } = null!;
        public string RangeOfSalary { get; set; } = null!;
        public string Skills { get; set; } = null!;
        public DateTime PosterDateCreate { get; set; }
        public string Place { get; set; } = null!;
        public bool Status { get; set; }
        public int PlanId { get; set; }
        public int JobId { get; set; }

        public virtual Job Job { get; set; } = null!;
        public virtual Plan Plan { get; set; } = null!;
    }
}
