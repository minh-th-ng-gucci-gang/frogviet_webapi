﻿using System;
using System.Collections.Generic;

namespace FrogViet.Domain.Entity
{
    public partial class Job
    {
        public Job()
        {
            Posts = new HashSet<Post>();
        }

        public int JobId { get; set; }
        public string JobName { get; set; } = null!;
        public string JobDecription { get; set; } = null!;
        public string JobRequirement { get; set; } = null!;
        public int RecruitmentNumber { get; set; }
        public string Education { get; set; } = null!;
        public int CompanyId { get; set; }

        public virtual Company Company { get; set; } = null!;
        public virtual ICollection<Post> Posts { get; set; }
    }
}
