﻿using System;
using System.Collections.Generic;

namespace FrogViet.Domain.Entity
{
    public partial class Event
    {
        public int EventId { get; set; }
        public string EventType { get; set; } = null!;
        public DateTime EventDate { get; set; }
        public byte[] EventTime { get; set; } = null!;
        public string EventLocation { get; set; } = null!;
        public string EventDescription { get; set; } = null!;
        public bool Status { get; set; }
        public int PlanId { get; set; }

        public virtual Plan Plan { get; set; } = null!;
    }
}
