﻿using System;
using System.Collections.Generic;

namespace FrogViet.Domain.Entity
{
    public partial class Plan
    {
        public Plan()
        {
            Events = new HashSet<Event>();
            Posts = new HashSet<Post>();
        }

        public int PlanId { get; set; }
        public string PlanName { get; set; } = null!;
        public string Decription { get; set; } = null!;
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public int UserId { get; set; }
        public DateTime DateCreate { get; set; }
        public bool Status { get; set; }

        public virtual UserAccount User { get; set; } = null!;
        public virtual ICollection<Event> Events { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}
