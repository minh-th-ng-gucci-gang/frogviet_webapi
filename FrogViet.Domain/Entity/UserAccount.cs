﻿using System;
using System.Collections.Generic;

namespace FrogViet.Domain.Entity
{
    public partial class UserAccount
    {
        public UserAccount()
        {
            Companies = new HashSet<Company>();
            Cvs = new HashSet<Cv>();
            Interviews = new HashSet<Interview>();
            Plans = new HashSet<Plan>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; } = null!;
        public string UserPassword { get; set; } = null!;
        public string UserEmail { get; set; } = null!;
        public long UserPhone { get; set; }
        public DateTime UserBirth { get; set; }
        public DateTime DateCreate { get; set; }
        public int Role { get; set; }
        public bool Status { get; set; }
        public long? IdcardNumber { get; set; }
        public string? Address { get; set; }
        public string? UserImage { get; set; }
        public DateTime? DateUpdate { get; set; }

        public virtual ICollection<Company> Companies { get; set; }
        public virtual ICollection<Cv> Cvs { get; set; }
        public virtual ICollection<Interview> Interviews { get; set; }
        public virtual ICollection<Plan> Plans { get; set; }
    }
}
