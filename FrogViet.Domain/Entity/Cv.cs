﻿using System;
using System.Collections.Generic;

namespace FrogViet.Domain.Entity
{
    public partial class Cv
    {
        public Cv()
        {
            Interviews = new HashSet<Interview>();
        }

        public int Cvid { get; set; }
        public string CvImage { get; set; } = null!;
        public string CvName { get; set; } = null!;
        public string JobTitle { get; set; } = null!;
        public string ContactInformation { get; set; } = null!;
        public string Summary { get; set; } = null!;
        public string Education { get; set; } = null!;
        public string WorkExperience { get; set; } = null!;
        public string Skills { get; set; } = null!;
        public string Certification { get; set; } = null!;
        public string Language { get; set; } = null!;
        public string Interests { get; set; } = null!;
        public string Email { get; set; } = null!;
        public DateTime DateCreate { get; set; }
        public bool Status { get; set; }
        public DateTime DateUpdate { get; set; }
        public int UserId { get; set; }

        public virtual UserAccount User { get; set; } = null!;
        public virtual ICollection<Interview> Interviews { get; set; }
    }
}
