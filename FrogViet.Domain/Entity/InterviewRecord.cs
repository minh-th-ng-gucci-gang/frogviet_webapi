﻿using System;
using System.Collections.Generic;

namespace FrogViet.Domain.Entity
{
    public partial class InterviewRecord
    {
        public int InterviewRecordId { get; set; }
        public string Notes { get; set; } = null!;
        public string QuestionAnswer { get; set; } = null!;
        public string Comment { get; set; } = null!;
        public int InterviewId { get; set; }

        public virtual Interview Interview { get; set; } = null!;
    }
}
