﻿using System;
using System.Collections.Generic;

namespace FrogViet.Domain.Entity
{
    public partial class InterviewResult
    {
        public int InterviewResultId { get; set; }
        public int Grade { get; set; }
        public DateTime DateResult { get; set; }
        public string Result { get; set; } = null!;
        public string Comment { get; set; } = null!;
        public int InterviewId { get; set; }

        public virtual Interview Interview { get; set; } = null!;
    }
}
