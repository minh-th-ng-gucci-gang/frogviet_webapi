﻿using System;
using System.Collections.Generic;

namespace FrogViet.Domain.Entity
{
    public partial class Company
    {
        public Company()
        {
            Jobs = new HashSet<Job>();
        }

        public int CompanyId { get; set; }
        public string CompanyName { get; set; } = null!;
        public string CompanyDecription { get; set; } = null!;
        public string? CompanyWeb { get; set; }
        public string CompanyAddress { get; set; } = null!;
        public string? CompanyEmail { get; set; }
        public long CompanyPhone { get; set; }
        public decimal CodeLicense { get; set; }
        public int UserId { get; set; }

        public virtual UserAccount User { get; set; } = null!;
        public virtual ICollection<Job> Jobs { get; set; }
    }
}
