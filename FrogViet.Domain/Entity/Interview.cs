﻿using System;
using System.Collections.Generic;

namespace FrogViet.Domain.Entity
{
    public partial class Interview
    {
        public Interview()
        {
            InterviewRecords = new HashSet<InterviewRecord>();
            InterviewResults = new HashSet<InterviewResult>();
        }

        public int InterviewId { get; set; }
        public DateTime InterviewDate { get; set; }
        public string InterviewLocation { get; set; } = null!;
        public string InterviewDecription { get; set; } = null!;
        public bool Status { get; set; }
        public int UserId { get; set; }
        public int Cvid { get; set; }

        public virtual Cv Cv { get; set; } = null!;
        public virtual UserAccount User { get; set; } = null!;
        public virtual ICollection<InterviewRecord> InterviewRecords { get; set; }
        public virtual ICollection<InterviewResult> InterviewResults { get; set; }
    }
}
